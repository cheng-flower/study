/**
 * @author 小海
 * @version JDK1.8/Mysql8.0/IDEA2020
 * @create 2021-11-03 23:08
 */


public class User {
    private String name;
    private char sex;

    public User() {
    }

    public User(String name, char sex) {
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                '}';
    }
}
